Hallo, {{$name}} | Event-Erinnerung
Es wurd ein neues Event hinzugefügt!
Name: {{$eventName}}
Start-Datum: {{$startDate}}
End-Datum: {{$endDate}}
Sie haben keinen DatePoll Account? Ignorieren Sie diese Email einfach.
Dies ist eine automatisierte Nachricht. Bitte antworten Sie nicht.