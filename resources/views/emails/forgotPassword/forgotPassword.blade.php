<html>
<body>
<h1>Hallo, {{$name}}</h1>
<p>Für deinen DatePoll-Account wurde ein Passwort Reset angefordert!</p>
<p>Kopiere folgenden Code und füge ihn auf der Website ein.</p>
<p>Code: <b>{{$code}}</b></p>
<p>Sie haben keinen DatePoll Account oder keinen Passwort Reset angefordert? Ignorieren Sie diese Email einfach.</p>
<i>Dies ist eine automatisierte Nachricht. Bitte antworten Sie nicht.</i>
</body>
</html>