<div align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/4xJBwve.png" alt="Project logo"></a>
</div>

<div align="center"><h3>DatePoll (Backend)</h3></div>

---

<div align="center">
  <p>DatePoll ist eine Vereinsmanagement Applikation welche versucht
  Vereinsstrukturen und das generelle Vereinsleben zu digitalisieren.</p>
</div>

## 📝 Table of Contents
- [Getting Started](#getting_started)
- [Documentation](#docs)
- [Built Using](#built_using)
- [Authors](#authors)

## 🏁 Getting Started <a name = "getting_started"></a>
If you want to deploy the DatePoll-Backend service please visit following
[page](https://datepoll.dafnik.me/installation/) .

## 🏁 API documentation <a name = "getting_started"></a>
If you want to learn more about the REST Api visit this
[page](https://datepoll.dafnik.me/API/) .

## ⛏️ Built Using <a name = "built_using"></a>
- [MariaDB](https://mariadb.org/) - Database
- [Laravel Lumen](https://lumen.laravel.com/) - Server Framework
- [Angular](https://angular.io/) - Web Framework
- [PHP](https://php.net) - Server Environment
- [Docker](https://docker.io) - Deployment system

## ✍️ Authors <a name = "authors"></a>
- [@Dafnik](https://gitlab.com/Dafnik)

See also the list of 
[contributors](https://gitlab.com/groups/BuergerkorpsEggenburg/-/group_members)
who participated in this project.
