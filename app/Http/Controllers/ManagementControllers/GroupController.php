<?php

namespace App\Http\Controllers\ManagementControllers;

use App\Http\Controllers\Controller;
use App\Models\Groups\Group;
use App\Models\Groups\UsersMemberOfGroups;
use App\Models\Subgroups\UsersMemberOfSubgroups;
use App\Models\User\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use stdClass;

class GroupController extends Controller
{

  /**
   * Display a listing of the resource.
   *
   * @return JsonResponse
   */
  public function getAll() {
    $groups = Group::all();
    foreach ($groups as $group) {
      $group->subgroups = $group->subgroups();
    }

    return response()->json([
      'msg' => 'List of all groups',
      'groups' => $groups], 200);
  }

  /**
   * Store a newly created resource in storage.
   *
   * @param Request $request
   * @return JsonResponse
   * @throws ValidationException
   */
  public function create(Request $request) {
    $this->validate($request, [
      'name' => 'required|max:190|min:1',
      'description' => 'max:65535']);

    $name = $request->input('name');
    $description = $request->input('description');

    $group = new Group([
      'name' => $name,
      'description' => $description]);

    if ($group->save()) {
      $response = [
        'msg' => 'Group created',
        'group' => $group];

      return response()->json($response, 201);
    }

    return response()->json(['msg' => 'An error occurred'], 500);
  }

  /**
   * Display the specified resource.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function getSingle($id) {
    $group = Group::find($id);
    if ($group == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    $group->subgroups = $group->subgroups();

    $usersToShow = [];

    $usersMembersOfGroups = $group->usersMemberOfGroups();
    foreach ($usersMembersOfGroups as $userMemberOfGroup) {
      $user = new stdClass();
      $user->id = $userMemberOfGroup->user()->id;
      $user->firstname = $userMemberOfGroup->user()->firstname;
      $user->surname = $userMemberOfGroup->user()->surname;
      $user->role = $userMemberOfGroup->role;

      $usersToShow[] = $user;
    }

    $group->users = $usersToShow;
    return response()->json([
      'msg' => 'Group information',
      'group' => $group]);
  }

  /**
   * Update the specified resource in storage.
   *
   * @param Request $request
   * @param int $id
   * @return JsonResponse
   * @throws ValidationException
   */
  public function update(Request $request, $id) {
    $this->validate($request, [
      'name' => 'required|max:255|min:1',
      'description' => 'max:65535']);

    $group = Group::find($id);

    if ($group == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    $name = $request->input('name');
    $description = $request->input('description');

    $group->name = $name;
    $group->description = $description;

    if ($group->save()) {
      $response = [
        'msg' => 'Group updated',
        'group' => $group];

      return response()->json($response, 201);
    }

    return response()->json(['msg' => 'An error occurred'], 500);
  }

  /**
   * Remove the specified resource from storage.
   *
   * @param int $id
   * @return JsonResponse
   */
  public function delete($id) {
    $group = Group::find($id);
    if ($group == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    if (!$group->delete()) {
      return response()->json(['msg' => 'Group deletion failed'], 500);
    }

    return response()->json(['msg' => 'Group deleted successfully'], 200);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   * @throws ValidationException
   */
  public function addUser(Request $request) {
    $this->validate($request, [
      'user_id' => 'required|integer',
      'group_id' => 'required|integer',
      'role' => 'max:190']);

    $userID = $request->input('user_id');
    $groupID = $request->input('group_id');
    $role = $request->input('role');

    if (!User::exists($userID)) {
      return response()->json(['msg' => 'User not found'], 404);
    }

    if (Group::find($groupID) == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    $userMemberOfGroup = UsersMemberOfGroups::where('group_id', $groupID)
                                            ->where('user_id', $userID)
                                            ->first();
    if ($userMemberOfGroup != null) {
      return response()->json(['msg' => 'User is already member of this group'], 201);
    }

    $userMemberOfGroup = new UsersMemberOfGroups([
      'user_id' => $userID,
      'group_id' => $groupID,
      'role' => $role]);

    if (!$userMemberOfGroup->save()) {
      return response()->json(['msg' => 'Could not add user to this group'], 500);
    }

    return response()->json([
      'msg' => 'Successfully added user to group',
      'userMemberOfGroup' => $userMemberOfGroup], 201);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   * @throws ValidationException
   */
  public function removeUser(Request $request) {
    $this->validate($request, [
      'user_id' => 'required|integer',
      'group_id' => 'required|integer']);

    $userID = $request->input('user_id');
    $groupID = $request->input('group_id');

    if (!User::exists($userID)) {
      return response()->json(['msg' => 'User not found'], 404);
    }

    if (Group::find($groupID) == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    $userMemberOfGroup = UsersMemberOfGroups::where('group_id', $groupID)
                                            ->where('user_id', $userID)
                                            ->first();
    if ($userMemberOfGroup == null) {
      return response()->json(['msg' => 'User is not a member of this group'], 201);
    }

    if (!$userMemberOfGroup->delete()) {
      return response()->json(['msg' => 'Could not remove user of this group'], 500);
    }

    /* Remove user from child subgroups */
    $userMemberOfSubgroupsToRemove = array();
    $userMemberOfSubgroups = UsersMemberOfSubgroups::where('user_id', $userID)
                                                   ->get();
    foreach ($userMemberOfSubgroups as $userMemberOfSubgroup) {
      if ($userMemberOfSubgroup->subgroup()->group_id = $groupID) {
        $userMemberOfSubgroupsToRemove[] = $userMemberOfSubgroup;
      }
    }

    foreach ($userMemberOfSubgroupsToRemove as $userMemberOfSubgroupToRemove) {
      if (!$userMemberOfSubgroupToRemove->delete()) {
        return response()->json(['msg' => 'Could not remove user of child subgroups'], 500);
      }
    }

    $response = [
      'msg' => 'Successfully removed user from group',
      'userMemberOfSubgroups' => $userMemberOfSubgroups,
      'addUser' => [
        'href' => 'api/v1/management/group/addUser',
        'method' => 'POST',
        'params' => 'group_id, user_id, role']];

    return response()->json($response, 200);
  }

  /**
   * @param Request $request
   * @return JsonResponse
   * @throws ValidationException
   */
  public function updateUser(Request $request) {
    $this->validate($request, [
      'user_id' => 'required|integer',
      'group_id' => 'required|integer',
      'role' => 'max:190']);

    $userID = $request->input('user_id');
    $groupID = $request->input('group_id');
    $role = $request->input('role');

    if (!User::exists($userID)) {
      return response()->json(['msg' => 'User not found'], 404);
    }

    if (Group::find($groupID) == null) {
      return response()->json(['msg' => 'Group not found'], 404);
    }

    $userMemberOfGroup = UsersMemberOfGroups::where('group_id', $groupID)
                                            ->where('user_id', $userID)
                                            ->first();
    if ($userMemberOfGroup == null) {
      return response()->json(['msg' => 'User is not a member of this group'], 404);
    }

    $userMemberOfGroup->role = $role;
    if (!$userMemberOfGroup->save()) {
      return response()->json(['msg' => 'Could not save UserMemberOfGroup'], 500);
    }

    $response = [
      'msg' => 'Successfully updated user in group',
      'userMemberOfGroup' => $userMemberOfGroup,
      'addUser' => [
        'href' => 'api/v1/management/group/addUser',
        'method' => 'POST',
        'params' => 'group_id, user_id, role']];

    return response()->json($response, 200);
  }

  /**
   * @param int $userID
   * @return JsonResponse
   */
  public function joined($userID) {
    $user = User::find($userID);
    if ($user == null) {
      return response()->json([
        'msg' => 'User not found',
        'error_code' => 'user_not_found'], 404);
    }

    $groupsToReturn = array();
    $userMemberOfGroups = UsersMemberOfGroups::where('user_id', $userID)
                                             ->get();
    foreach ($userMemberOfGroups as $userMemberOfGroup) {
      $group = $userMemberOfGroup->group();
      $groupsToReturn[] = $group;
    }

    return response()->json([
      'msg' => 'List of joined groups',
      'groups' => $groupsToReturn], 200);
  }

  /**
   * @param int $userID
   * @return JsonResponse
   */
  public function free($userID) {
    $user = User::find($userID);
    if ($user == null) {
      return response()->json([
        'msg' => 'User not found',
        'error_code' => 'user_not_found'], 404);
    }

    $allGroups = Group::all();
    $groupsToReturn = array();
    $userMemberOfGroups = UsersMemberOfGroups::where('user_id', $userID)
                                             ->get();
    foreach ($allGroups as $group) {
      $isInGroup = false;
      foreach ($userMemberOfGroups as $userMemberOfGroup) {
        if ($userMemberOfGroup->group()->id == $group->id) {
          $isInGroup = true;
          break;
        }
      }

      if (!$isInGroup) {
        $groupsToReturn[] = $group;
      }
    }

    return response()->json([
      'msg' => 'List of free groups',
      'groups' => $groupsToReturn], 200);
  }
}
